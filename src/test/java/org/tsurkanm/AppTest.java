package org.tsurkanm;

import static org.junit.Assert.assertEquals;
import static org.tsurkanm.App.generateList;
import static org.tsurkanm.App.multiply;

import org.junit.Test;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Unit test for simple App.
 */
public class AppTest {
    private static final Logger log = LoggerFactory.getLogger(AppTest.class);

    @BeforeAll
    static void setup() {
        log.info("@BeforeAll - executes once before all test methods in this class");
    }

    @BeforeEach
    void init() {
        log.info("@BeforeEach - executes before each test method in this class");
    }

    @AfterEach
    void tearDown() {
        log.info("@AfterEach - executed after each test method.");
    }

    @AfterAll
    static void done() {
        log.info("@AfterAll - executed after all test methods.");
    }

    @Test
    public void testGenerateList() {
        assertEquals(Arrays.asList(1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0), generateList(1, 10, 1));
        assertEquals(Arrays.asList(1.0, 4.0, 7.0, 10.0), generateList(1, 10, 3));
        assertEquals(Arrays.asList(-10.0, -7.0, -4.0, -1.0, 2.0, 5.0, 8.0), generateList(-10, 10, 3));
    }

    @Test
    public void testMultiply() {
        List<Double> a = generateList(-10, 10, 3);
        List<Double> b = generateList(-10, 10, 3);

        assertEquals(Arrays.asList(100.0, 49.0, 16.0, 1.0, 4.0, 25.0, 64.0), multiply(a, b));
    }

    @Test
    public void testMultiplyWithListsDifferentLengths() {
        List<Double> a = generateList(-10, 10, 3);
        List<Double> b = generateList(1, 10, 3);

        assertEquals(Collections.emptyList(), multiply(a, b));
    }
}
