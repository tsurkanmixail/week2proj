package org.tsurkanm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

public class App {
    private static final Logger logger = LoggerFactory.getLogger(App.class);
    private static final String ACTION = "{} * {} = {}";

    public static void main(String[] args) {
        //Читаємо .properties. Файл повинен лежати в папці з якої запускаєте jar!!!
        logger.debug("Search path for config.properties file.");
        String appConfigPath = "";
        try {
            appConfigPath = new File(".").getCanonicalPath() + File.separator + "config.properties";
        } catch (IOException e) {
            logger.warn(e.getMessage());
        }
        logger.debug("Properties file path defined.");

        Properties appProps = new Properties();

        logger.debug("Open file with properties.");
        try (InputStreamReader input = new InputStreamReader(new FileInputStream(appConfigPath), StandardCharsets.UTF_8)) {
            appProps.load(input);
        } catch (FileNotFoundException e) {
            logger.error("Not found config.properties file.");
            System.exit(0);
        } catch (IOException e) {
            logger.warn(e.getMessage());
        }

        logger.debug("Start reading properties.");
        String type = System.getProperty("type") == null ? "integer" : System.getProperty("type");
        String min = appProps.getProperty("minimum") == null ? "1" : appProps.getProperty("minimum");
        String max = appProps.getProperty("maximum") == null ? "10" : appProps.getProperty("maximum");
        String inc = appProps.getProperty("increment") == null ? "1" : appProps.getProperty("increment");
        logger.debug("Properties defined.");

        logger.debug("Generating a list of numbers.");
        List<Double> a = generateList(Double.parseDouble(min), Double.parseDouble(max), Double.parseDouble(inc));
        printResult(a, a, multiply(a, a), type);
        logger.debug("The end.\n");
    }

    public static List<Double> generateList(double min, double max, double inc) {
        logger.debug("Run generate double list.");
        List<Double> arr = new ArrayList<>();
        double temp = min;
        while (temp <= max) {
            arr.add(temp);
            temp += inc;
        }
        logger.debug("End generate double list.");
        return arr;
    }

    public static List<Double> multiply(List<Double> list1, List<Double> list2) {
        logger.debug("Run multiply two list.");
        List<Double> arr = new ArrayList<>();
        if (list1.size() != list2.size()) {
            logger.error("Lists of different lengths.");
            return Collections.emptyList();
        }
        for (int i = 0; i < list1.size(); i++) {
            arr.add(list1.get(i) * list2.get(i));
        }
        logger.debug("End multiply two list.");
        return arr;
    }

    public static void printResult(List<Double> list1, List<Double> list2, List<Double> result, String type) {
        logger.debug("Run print result.");
        if (result.isEmpty()) {
            logger.warn("Can't print result to console! Different length arrays!");
            return;
        }
        switch (type.toLowerCase()) {
            case "byte":
                for (int i = 0; i < result.size(); i++) {
                    logger.info(ACTION, list1.get(i).byteValue(), list2.get(i).byteValue(), result.get(i).byteValue());
                }
                break;
            case "integer":
                for (int i = 0; i < result.size(); i++) {
                    logger.info(ACTION, list1.get(i).intValue(), list2.get(i).intValue(), result.get(i).intValue());
                }
                break;
            case "double":
                for (int i = 0; i < result.size(); i++) {
                    logger.info(ACTION, list1.get(i), list2.get(i), result.get(i));
                }
                break;
            case "short":
                for (int i = 0; i < result.size(); i++) {
                    logger.info(ACTION, list1.get(i).shortValue(), list2.get(i).shortValue(), result.get(i).shortValue());
                }
                break;
            case "float":
                for (int i = 0; i < result.size(); i++) {
                    logger.info(ACTION, list1.get(i).floatValue(), list2.get(i).floatValue(), result.get(i).floatValue());
                }
                break;
            case "long":
                for (int i = 0; i < result.size(); i++) {
                    logger.info(ACTION, list1.get(i).longValue(), list2.get(i).longValue(), result.get(i).longValue());
                }
                break;
            default:
                logger.error("No such data type!!!");
        }
        logger.debug("End print result.");
    }
}
